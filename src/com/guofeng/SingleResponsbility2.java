package com.guofeng;

/**
 * @author guofeng
 * @Classname 单一职正确案例
 * @Description TODO
 * @Date 2021/5/2 15:48
 */
public class SingleResponsbility2 {
    public static void main(String[] args) {
        RoadVehicle roadVehicle = new RoadVehicle();
        roadVehicle.run("劳斯莱斯");
        AirVehicle airVehicle = new AirVehicle();
        airVehicle.run("F15战斗机");
    }


}
class RoadVehicle{
    public void run(String name) {
        System.out.println(name+"在公路上运行");
    }
}

class AirVehicle{
    public void run(String name) {
        System.out.println(name+"在天空上运行");
    }
}
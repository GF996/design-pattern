package com.guofeng;

/**
 * @author guofeng
 * @Classname 单一职责原则
 * @Description TODO
 * @Date 2021/5/2 15:12
 */

/**
 * 个人理解：单一职责原则，保证这个类只描述一件事情，当多个工功能共用并有冲突时可以用继承来保证一个类只描述一件事情。
 * 提高类的可读性和可维护性，降低耦合避免更改某一个功能引起的连锁反应。（逻辑简单代码量少的系统另说）
 */
public class Singleresponsibility {
    public static void main(String[] args) {
        Vehicle vehicle = new Vehicle();
        vehicle.run("飞机");
        vehicle.run("火车");
        vehicle.run("汽车");
    }
}

/**
 * 交通工具类
 * 违法了单一职责原则
 * 不同的交通工具分解成不同的类即可
 */
class Vehicle{
    public void run(String name){
        System.out.println(name + "在公路上跑========");
    }
}
